#!/bin/bash
# Read values from sensors and write to files

# Configuration file
CONFIGFILE=~/.config/sensorlog.ini

# Script for reading values
SENSORLOGITEM=sensorlogitem.sh

# File for last values
FILELAST=~/sensorlog-last.csv

# File for historic values
FILEHISTORY=~/sensorlog.csv

# Read configuration from file
if [ -f "$CONFIGFILE" ]; then
    . "$CONFIGFILE"
fi

# Read values from sensors
"$SENSORLOGITEM" >$FILELAST

# Append to history
cat $FILELAST >>$FILEHISTORY

