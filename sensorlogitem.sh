#!/bin/bash
# Fetch values from sensors and print them to stdout

# Config file
CONFIGFILE=~/.config/sensorlog.ini

# Script for reading values from sensors
READSENS=sensors.sh

# Read configuration
if [ -f "$CONFIGFILE" ]; then
    . "$CONFIGFILE"
fi

# Read values:
VAL085=`$READSENS -s BMP -t all`
VALDHT=`$READSENS -s DHT -t all`
# - temperature from BMP085
TEMP085=`echo $VAL085 | cut -f 1 -d ";"`
# - pressure from BMP085
PRESS085=`echo $VAL085 | cut -f 3 -d ";"`
# - temperature from DHT
TEMPDHT=`echo $VALDHT | cut -f 1 -d ";"`
# - humidity from DHT
HUMDHT=`echo $VALDHT | cut -f 2 -d ";"`

# Write data
NOW=`date +"%Y-%m-%d %H:%M:%S"`
echo "$NOW;$TEMP085;$PRESS085;$TEMPDHT;$HUMDHT"

